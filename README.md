# Modèle de microsimulation Autonomix Etablissements 

Ce dossier fournit les programmes permettant de former le package contenant le modèle de microsimulation Autonomix version 1 de juillet 2022.

[Lien vers la documentation du modèle](https://drees_code.gitlab.io/public/modeles/autonomix/documentation-atnmxetablissement/description-datnmx-%C3%A9tablissement.html)

Présentation de la Drees : La Direction de la recherche, des études, de l'évaluation et des statistiques (DREES) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères. 
https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees

Source de données : Ce package utilise les données de la base CARE Etablissement 2016. Traitements : Drees.

Date de la dernière exécution des programmes avant publication, et version des logiciels utilisés : Les programmes ont été exécutés le 19/07/2022 avec la version 4.0.3 de R. Les packages mobilisés sont listés dans le fichier DESCRIPTION du package. 
