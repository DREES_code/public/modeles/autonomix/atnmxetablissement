# Copyright (C) 2021-2022. Logiciel élaboré par l'État, via la Drees.

# Pole Simulation du bureau Handicap et Dépendance, Drees.

# Ce programme informatique a été développé par la Drees. Il permet d'exécuter le modèle de microsimulation Autonomix version 1.0 de juillet 2022.

# Ce programme a été exécuté le 19/07/2022 avec la version 4.0.3 de R et les packages mobilisés sont listés dans le fichier DESCRIPTION du projet »

# La documentation du modèle peut être consultés sur [gitlab](https://drees_code.gitlab.io/public/modeles/autonomix/documentation-atnmxetablissement/description-datnmx-%C3%A9tablissement.html)

# Ce programme utilise les données de la base CARE Institution 2016

# Bien qu'il n'existe aucune obligation légale à ce sujet, les utilisateurs de ce programme sont invités à signaler à la DREES leurs travaux issus de la réutilisation de ce code, ainsi que les éventuels problèmes ou anomalies qu'ils y rencontreraient, en écrivant à DREES-CODE@sante.gouv.fr

# Ce logiciel est régi par la licence EUPL V1.2 soumise au droit français et européen, respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence EUPL V1.2, telle que diffusée sur https://eupl.eu/1.2/fr/ et reproduite dans le fichier LICENCE diffusé conjointement au présent programme.
# En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.
# À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.


#' @keywords internal
"_PACKAGE"

# The following block is used by usethis to automatically manage
# roxygen namespace tags. Modify with care!
## usethis namespace: start
## usethis namespace: end
NULL
